<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tentukan_nilai</title>
</head>
<body>
    <?php
        function tentukan_nilai($number)
        {
            if ($number >= 85 && $number <= 100) {
                return "Sangat Baik";
            }
            elseif ($number >= 70 && $number < 85) {
                return "Baik";
            }
            elseif ($number >= 60 && $number < 70) {
                return "Cukup";
            }else {
                return "Kurang";
            }
            
        }

        //TEST CASES
        echo tentukan_nilai(98)."<br>"; //Sangat Baik
        echo tentukan_nilai(76)."<br>"; //Baik
        echo tentukan_nilai(67)."<br>"; //Cukup
        echo tentukan_nilai(43)."<br>"; //Kurang
    ?>
</body>
</html>